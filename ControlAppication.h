#ifndef CONTROLAPPLICATION_H
#define CONTROLAPPLICATION_H
#include <stdio.h>
#include <string>
#include <ShObjIdl.h>
#include <UIAutomation.h>
#include "Timer.h"
using namespace std;

class ControlApp {

public:
	ControlApp(LPWSTR appId, LPWSTR appName);
	void ActivateApp();
	IUIAutomationElement* FindElementByClassName(IUIAutomationElement* parent, const std::wstring& className, TreeScope scope);
	IUIAutomationElement* FindElementByName(IUIAutomationElement* parent, const std::wstring& name, TreeScope scope);
	IUIAutomationElement* FindElementById(IUIAutomationElement* parent, const std::wstring& automationId, TreeScope scope);
	IUIAutomationElement* GetParentElement();
	bool waitElementById(LPWSTR idElement, IUIAutomationElement* root, int timeout);
	IUIAutomationInvokePattern*	UsingInvokePattern(IUIAutomationElement* element);
	bool clickOnElement(IUIAutomationElement* element);
	void closeApplication();
	virtual ~ControlApp();

public:
	IUIAutomationElement* parentElement;
	LPWSTR appName;
	LPWSTR appId;

private:
	IUIAutomation* automation;

};
#endif