#ifndef TIMER_H
#define TIMER_H
#include "stdafx.h"
#include <UIAutomation.h>

class Timer {

public:
	Timer();
	~Timer();
	void startTimeout();
	bool isFinishTimeout(int timeout);

private:
	SYSTEMTIME lpSystemTimeStart;
	SYSTEMTIME lpSystemTimeEnd;
};
#endif