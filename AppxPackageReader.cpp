#include "stdafx.h"
#include <windows.h>
#include <shlwapi.h>
#include <UIAnimation.h>
#include "AppxPackageReader.h"
#include <AppxPackaging.h>

AppxPackageReader::AppxPackageReader(LPWSTR appPath):
	packageReader(NULL), manifestReader(NULL), appUserId(NULL)
{
	this->appPath = appPath;
}

LPWSTR AppxPackageReader::GetAppName(){

	if(appName == NULL){
		wprintf(L"Before execute method GetAppUserId");
	}

	return appName;
}

LPWSTR AppxPackageReader :: GetAppUserId(){
	HRESULT hrResult;
	
	if(SUCCEEDED(CoInitializeEx(NULL, COINIT_MULTITHREADED))){
        
		// Create a package reader using the file name given in command line
		hrResult = GetPackageReader(this->appPath, &packageReader);

        // Get manifest reader for the package and read from the manifest
        if (SUCCEEDED(hrResult))
        {
            hrResult = packageReader->GetManifest(&manifestReader);
			//IAppxManifestProperties* properties;
			//manifestReader->GetProperties(&properties);
        }

        if (SUCCEEDED(hrResult))
        {
			hrResult = ReadManifestApplications(manifestReader);
        }

	}



	return appUserId;
}

//
// Creates an app package reader.
//
// Parameters:
//   inputFileName 
//     Fully qualified name of the app package (.appx file) to be opened.
//   reader 
//     On success, receives the created instance of IAppxPackageReader.
//
HRESULT AppxPackageReader::GetPackageReader(
    _In_ LPCWSTR inputFileName,
    _Outptr_ IAppxPackageReader** reader)
{
    HRESULT hr = S_OK;
    IAppxFactory* appxFactory = NULL;
    IStream* inputStream = NULL;

    // Create a new factory
    hr = CoCreateInstance(
            __uuidof(AppxFactory),
            NULL,
            CLSCTX_INPROC_SERVER,
            __uuidof(IAppxFactory),
            (LPVOID*)(&appxFactory));

    // Create a stream over the input app package
    if (SUCCEEDED(hr))
    {
        hr = SHCreateStreamOnFileEx(
                inputFileName,
                STGM_READ | STGM_SHARE_EXCLUSIVE,
                0, // default file attributes
                FALSE, // do not create new file
                NULL, // no template
                &inputStream);
    }

    // Create a new package reader using the factory.  For 
    // simplicity, we don't verify the digital signature of the package.
    if (SUCCEEDED(hr))
    {
        hr = appxFactory->CreatePackageReader(
                inputStream,
                reader);
    }

    // Clean up allocated resources
    if (inputStream != NULL)
    {
        inputStream->Release();
        inputStream = NULL;
    }
    if (appxFactory != NULL)
    {
        appxFactory->Release();
        appxFactory = NULL;
    }
    return hr;
}

//
// Reads a subset of the manifest Applications element.
//
HRESULT AppxPackageReader :: ReadManifestApplications(
    _In_ IAppxManifestReader* manifestReader)
{
    HRESULT hr = S_OK;
    BOOL hasCurrent = FALSE;
    UINT32 applicationsCount = 0;

    IAppxManifestApplicationsEnumerator* applications = NULL;

    // Get elements and attributes from the manifest reader
    hr = manifestReader->GetApplications(&applications);
    if (SUCCEEDED(hr))
    {
        hr = applications->GetHasCurrent(&hasCurrent);

        while (SUCCEEDED(hr) && hasCurrent)
        {
            IAppxManifestApplication* application = NULL;
            LPWSTR applicationName = NULL;

            hr = applications->GetCurrent(&application);
            if (SUCCEEDED(hr))
            {
				application->GetStringValue(L"DisplayName", &appName);
				wprintf(L"\nApp name : %s\n", appName);
				application->GetAppUserModelId(&appUserId);
				wprintf(L"AppUserModelId : %s\n", appUserId);
            }
            if (SUCCEEDED(hr))
            {
                applicationsCount++;
				wprintf(L"App #%u: %s\n", applicationsCount, appName);
            }

            if (SUCCEEDED(hr))
            {
                hr = applications->MoveNext(&hasCurrent);
            }
            if (application != NULL)
            {
                application->Release();
                application = NULL;
            }
            CoTaskMemFree(applicationName);
        }

        wprintf(L"Count of apps in the package: %u\n", applicationsCount);
    }

    // Clean up allocated resources
    if (applications != NULL)
    {
        applications->Release();
        applications = NULL;
    }

    return hr;
}

//
// Reads a subset of the manifest Properties element.
//
//
HRESULT AppxPackageReader :: ReadManifestProperties(
    _In_ IAppxManifestReader* manifestReader)
{
    HRESULT hr = S_OK;

    IAppxManifestProperties* properties = NULL;

    // Get elements and attributes from the manifest reader
    hr = manifestReader->GetProperties(&properties);
    if (SUCCEEDED(hr))
    {
        LPWSTR displayName = NULL;
        LPWSTR description = NULL;

        hr = properties->GetStringValue(L"DisplayName", &displayName);

        if (SUCCEEDED(hr))
        {
            hr = properties->GetStringValue(L"Description", &description);
        }
        if (SUCCEEDED(hr))
        {
            wprintf(L"Package display name: %s\n", displayName);
            wprintf(L"Package description:  %s\n", description);
        }

        // Free all string buffers returned from the manifest API
        CoTaskMemFree(displayName);
        CoTaskMemFree(description);
    }
    // Clean up allocated resources
    if (properties != NULL)
    {
        properties->Release();
        properties = NULL;
    }

    return hr;
}

//
// Reads a subset of the manifest Identity element.
//
//
HRESULT AppxPackageReader :: ReadManifestPackageId(
    _In_ IAppxManifestReader* manifestReader)
{
    HRESULT hr = S_OK;

    IAppxManifestPackageId* packageId = NULL;
	
    // Get elements and attributes from the manifest reader
    hr = manifestReader->GetPackageId(&packageId);
    if (SUCCEEDED(hr))
    {
        LPWSTR packageFullName = NULL;
        LPWSTR packageName = NULL;
        UINT64 packageVersion = 0;

        hr = packageId->GetPackageFullName(&appUserId);
		
        if (SUCCEEDED(hr))
        {
			hr = packageId->GetName(&appName);
        }
        if (SUCCEEDED(hr))
        {
            hr = packageId->GetVersion(&packageVersion);
        }
        if (SUCCEEDED(hr))
        {
            wprintf(L"Package full name: %s\n", appUserId);
			wprintf(L"Package name: %s\n", appName);
            wprintf(L"Package version: ");

            // Convert version number from 64-bit integer to dot-quad form
            for (int bitPosition = 0x30; bitPosition >= 0; bitPosition -= 0x10)
            {
                UINT64 versionWord = (packageVersion >> bitPosition) & 0xFFFF;
                wprintf(L"%llu.", versionWord);
            }
            wprintf(L"\n");
        }
		
        // Free all string buffers returned from the manifest API
        //CoTaskMemFree(packageFullName);
        //CoTaskMemFree(packageName);
    }

    // Clean up allocated resources
    if (packageId != NULL)
    {
        packageId->Release();
        packageId = NULL;
    }
	 
    return hr;
}

AppxPackageReader::~AppxPackageReader()
{
	if (manifestReader != NULL)
	{
		manifestReader->Release();
		manifestReader = NULL;
	}
	if (packageReader != NULL)
	{
		packageReader->Release();
		packageReader = NULL;
	}
	CoUninitialize();
	wprintf(L"\nAppxPackageReader* delete");
}
