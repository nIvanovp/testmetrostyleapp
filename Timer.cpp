#include "stdafx.h"
#include "Timer.h"

Timer::Timer() { }

void Timer::startTimeout() {
	GetSystemTime(&lpSystemTimeStart);
}

bool Timer::isFinishTimeout(int timeout) {
	GetSystemTime(&lpSystemTimeEnd);
	int endTime = lpSystemTimeEnd.wMinute*60000 + lpSystemTimeEnd.wSecond*1000 + lpSystemTimeEnd.wMilliseconds;
	int startTime = lpSystemTimeStart.wMinute*60000 + lpSystemTimeStart.wSecond*1000 + lpSystemTimeStart.wMilliseconds;
	int elapsed_time = endTime - startTime;
	if( elapsed_time >= timeout ) {
		return true;
	}
	return false;
}

Timer::~Timer() { }
