#pragma once
#ifndef APPXPACKAGEREADER_H
#define APPXPACKAGEREADER_H
#include <AppxPackaging.h>

class AppxPackageReader
{

public:
	AppxPackageReader(LPWSTR appPath);
	~AppxPackageReader();

public:
	LPWSTR GetAppUserId();
	LPWSTR GetAppName();

private:
	HRESULT GetPackageReader(_In_ LPCWSTR inputFileName, _Outptr_ IAppxPackageReader** reader);
	HRESULT ReadManifestPackageId(_In_ IAppxManifestReader* manifestReader);
	HRESULT ReadManifestProperties(_In_ IAppxManifestReader* manifestReader);
	HRESULT ReadManifestApplications(_In_ IAppxManifestReader* manifestReader);
	
private:
	IAppxPackageReader* packageReader;
    IAppxManifestReader* manifestReader;
	LPWSTR appUserId;
	LPWSTR appVersion;
	LPWSTR appName;
	LPWSTR appPath;

};

#endif

