// SampleTest.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ControlAppication.h"

int _tmain(int argc, _TCHAR* argv[]) {
	if ( argc > 2 )	{
        wprintf(L"\n Usage: NookUiScreenShot.exe inputFile");
        wprintf(L"\n inputFile: Path to the app package to read (.appx)\n");
        return 0;
    }

	//AppxPackageReader* packageReader = new AppxPackageReader(argv[1]);
	LPWSTR appId = L"BarnesNoble.Nook_ahnzqzva31enc!App"; //packageReader->GetAppUserId();
	LPWSTR appName = L"BarnesNoble.Nook"; //packageReader->GetAppName();

	if( appId == NULL )	{
		wprintf(L"\nAppUserModelId : is not found\n");	
		return 0;
	}

	wprintf(L"\nApplication info : name '%s', id '%s'\n", appName, appId);

	ControlApp* controlApp = new ControlApp(appId, appName);
	controlApp->ActivateApp();
	
	IUIAutomationElement* root = controlApp->GetParentElement();
	if(controlApp->waitElementById(L"MicrosoftAccountButton", root, 10000)) {
		Sleep(1000);
		IUIAutomationElement* button = controlApp->FindElementById(root, L"MicrosoftAccountButton", TreeScope_Descendants);
		controlApp->clickOnElement(button);
		Sleep(5000);
	}

	//packageReader->~AppxPackageReader();
	controlApp->closeApplication();
	controlApp->~ControlApp();

	return 0;
}

