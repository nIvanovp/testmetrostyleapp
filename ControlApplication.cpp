#include "stdafx.h"
#include "ControlAppication.h"

ControlApp::ControlApp(LPWSTR appId, LPWSTR appName) : parentElement(NULL) {
	HRESULT hrResult;

	if (SUCCEEDED(CoInitializeEx(NULL, COINIT_MULTITHREADED))) {
		hrResult = CoCreateInstance(__uuidof(CUIAutomation8), NULL, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&(this->automation)));
            if (FAILED(hrResult))
                wprintf(L"Failed to create a CUIAutomation8, HR: 0x%08x\n", hrResult);
	}
	this->appId = appId;
	this->appName = appName;
}

void ControlApp::ActivateApp() {
	IApplicationActivationManager* paam = nullptr;
	 DWORD pid=0;
	 if( FAILED(CoCreateInstance(CLSID_ApplicationActivationManager, nullptr, CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&paam)))
		 || FAILED(paam->ActivateApplication(this->appId, nullptr, AO_NONE, &pid)) ) {
			 wprintf(L"\nFailed activation application.");
			 return;
	 }

	 (this->automation)->GetRootElement(&(this->parentElement));
	 Sleep(2000);
}

IUIAutomationElement* ControlApp::GetParentElement() {
	return this->parentElement;
}

void ControlApp::closeApplication() {
	keybd_event(VK_MENU, 0x12, 0, 0); // Alt Press
	keybd_event(VK_F4, 0x73, 0, 0); // F4 Press
	keybd_event(VK_F4, 0x73, KEYEVENTF_KEYUP, 0); // F4 Release
	keybd_event(VK_MENU, 0x12, KEYEVENTF_KEYUP, 0); // Alt Release
}

bool ControlApp::clickOnElement(IUIAutomationElement* element) {
	if( element == NULL )
		return false;

	IUIAutomationInvokePattern* invokePattern = UsingInvokePattern(element);
	if( invokePattern != NULL ) {
		HRESULT hr = invokePattern->Invoke();
		if( FAILED(hr) ) 
			wprintf(L"\nFailed invoke on element");
		else
			wprintf(L"\nClick on element");
		invokePattern->Release();
	} else
		return false;

	if(element != NULL)
		element->Release();

	return true;
}

bool ControlApp::waitElementById(LPWSTR idElement, IUIAutomationElement* root, int timeout) {
	Timer* timer = new Timer();
	bool isWaitState = true;
	IUIAutomationElement* waitElement = FindElementById(root, idElement, TreeScope_Descendants);

	wprintf(L"\nPlease wait the item '%s'.", idElement);
	wprintf(L"\nWaiting...");

	timer->startTimeout();
	while(waitElement == NULL) {
		waitElement = FindElementById(root, idElement, TreeScope_Descendants);
		if(timer->isFinishTimeout(timeout)) 
			break;
	}
	
	wprintf(L"\nThanks for waiting...");

	if(waitElement == NULL) {
		isWaitState = false;
		wprintf(L"\nElement is not found");
	}

	timer->~Timer();
	return isWaitState;
}

IUIAutomationElement* ControlApp::FindElementByClassName(IUIAutomationElement* parent, const std::wstring& className, TreeScope scope) {
	IUIAutomationElement* foundElement = NULL;
	IUIAutomationCondition* nameCond;
	BSTR bstrName = SysAllocStringLen(className.data(), className.size());
	VARIANT nameVar;
	nameVar.vt = VT_BSTR;
	nameVar.bstrVal = bstrName;
	(this->automation)->CreatePropertyCondition(UIA_ClassNamePropertyId, nameVar, &nameCond);
	parent->FindFirst(scope, nameCond, &foundElement);

	if(foundElement == NULL)
		wprintf(L"\nUIElement with className %s is not found!", className);
	
	return foundElement;
}

IUIAutomationElement* ControlApp::FindElementByName(IUIAutomationElement* parent, const std::wstring& name, TreeScope scope) {
	IUIAutomationElement* foundElement = nullptr;
	IUIAutomationCondition* nameCond;
	BSTR bstrName = SysAllocStringLen(name.data(), name.size());
	VARIANT nameVar;
	nameVar.vt = VT_BSTR;
	nameVar.bstrVal = bstrName;
	(this->automation)->CreatePropertyCondition(UIA_NamePropertyId, nameVar, &nameCond);
	parent->FindFirst(scope, nameCond, &foundElement);
	
	if(foundElement == NULL)
		wprintf(L"\nUIElement with name ='%s' is not found!", name.c_str());
	
	return foundElement;
}

IUIAutomationElement* ControlApp::FindElementById(IUIAutomationElement* parent, const std::wstring& automationId, TreeScope scope) {
	IUIAutomationElement* foundElement = NULL;
	IUIAutomationCondition* nameCond;
	BSTR bstrName = SysAllocStringLen(automationId.data(), automationId.size());
	VARIANT nameVar;
	nameVar.vt = VT_BSTR;
	nameVar.bstrVal = bstrName;
	(this->automation)->CreatePropertyCondition(UIA_AutomationIdPropertyId, nameVar, &nameCond);
	parent->FindFirst(scope, nameCond, &foundElement);
	
	if( foundElement == NULL )
		wprintf(L"\nUIElement with id='%s' is not found!", automationId.c_str());

	return foundElement;
}

IUIAutomationInvokePattern* ControlApp::UsingInvokePattern(IUIAutomationElement* element) {
	IUIAutomationInvokePattern *patternInvoke = NULL;
	if( element == NULL ) {
		wprintf(L"\nUIElement == NULL");
		return NULL;
	}

	element->GetCurrentPatternAs(UIA_InvokePatternId, IID_PPV_ARGS(&patternInvoke));
	if( patternInvoke == NULL )	{
		wprintf(L"\nInvokePattern == null");
		return NULL;
	}

	return patternInvoke;
}

ControlApp::~ControlApp() {
	CoUninitialize();
}